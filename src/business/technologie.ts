export class Technologie {
  constructor(readonly nom: string, readonly icon?: string, readonly iconImg?: IconImage) { };
}

interface IconImage {
  file: string;
  width: number;
  height: number;
}

export const ADA: Technologie = { nom: 'Ada', iconImg: { file: 'techno-logo-ada.png', width: 50, height: 50 } };
export const C: Technologie = { nom: 'C', iconImg: { file: 'techno-logo-c.png', width: 60, height: 40 } };
export const CSS: Technologie = { nom: 'CSS', iconImg: { file: 'techno-logo-css.jpg', width: 40, height: 50 } };
export const HTML: Technologie = { nom: 'HTML', iconImg: { file: 'techno-logo-html.png', width: 40, height: 50 } };
export const JAVA: Technologie = { nom: 'Java', iconImg: { file: 'techno-logo-java.png', width: 50, height: 20 } };
export const JAVASCRIPT: Technologie = { nom: 'JavaScript', iconImg: { file: 'techno-logo-js.png', width: 40, height: 40 } };
export const PYTHON: Technologie = { nom: 'Python', iconImg: { file: 'techno-logo-python.jpg', width: 64, height: 20 } };

export const TECHNOLOGIES: Technologie[] = [ADA, C, CSS, HTML, JAVA, JAVASCRIPT, PYTHON];
