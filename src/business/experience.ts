import { Equipe } from './equipe';
import { Periode } from './periode';
import { Technologie } from './technologie';
import { TypeProjet } from './type-projet';

export interface Experience {
    readonly periode: Periode,
    readonly sujet: string,
    readonly technos: Array<Technologie>,
    readonly equipe: Equipe,
    readonly typeProjet: TypeProjet
}

export function sujetHTML(xp: Experience): string {
  // TODO Coder.
  return xp.sujet; // .includes("Ministère de l'Agriculture") ? xp.sujet.replace("Ministère de l'Agriculture", "<a href='https://agriculture.gouv.fr/'>Ministère de l'Agriculture</a>") : xp.sujet;
}
