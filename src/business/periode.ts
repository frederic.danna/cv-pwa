import { Duree } from './duree';
export interface Periode {
  debut: Date;
  duree: Duree;
}
