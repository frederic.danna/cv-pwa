export interface Duree {
  qte: number;
  unit: UniteDuree;
}

export class UniteDuree {

  constructor(readonly singulier: string, readonly pluriel: string) { };

  nbrJours(unite: UniteDuree): number {
    const mult: number | undefined =
      unite === JOUR ? 1 :
        unite === SEMAINE ? 7 :
          unite === MOIS ? 30 :
            unite === ANNEE ? 220 :
              undefined;
    if (mult === undefined)
      throw new Error("Cas non géré : conversion de " + unite + " en jours.");
    return mult;
  }
}

export const JOUR = new UniteDuree("jour", "jours");
export const SEMAINE = new UniteDuree("semaine", "semaines");
export const MOIS = new UniteDuree("mois", "mois");
export const ANNEE = new UniteDuree("année", "années");
