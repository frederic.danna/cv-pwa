import { Periode } from './periode';

export interface Formation {
  readonly date_debut?: Date,
  readonly date_obtention?: Date,
  readonly titre: string,
  readonly ecole?: string,
  readonly universite?: string,
}

export function titreHTML(form: Formation): string {
  // TODO Coder.
  return form.titre; // .includes("Ministère de l'Agriculture") ? xp.sujet.replace("Ministère de l'Agriculture", "<a href='https://agriculture.gouv.fr/'>Ministère de l'Agriculture</a>") : xp.sujet;
}
