import { DatePipe, LocationStrategy } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { DomSanitizer, Title } from '@angular/platform-browser';
import * as d3 from 'd3';
import { ANNEE, JOUR, MOIS, SEMAINE } from '../business/duree';
import { Experience, sujetHTML } from '../business/experience';
import { Formation } from '../business/formation';
import { ADA, C, CSS, HTML, JAVA, JAVASCRIPT, PYTHON, Technologie, TECHNOLOGIES } from '../business/technologie';
import { TypeProjet } from '../business/type-projet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  readonly BASE_HREF: string;

  readonly INFOS_GENERALES = {
    prenom: 'Antoine',
    nom: 'Danna',
    date_naissance: new Date("2001-06-07")
  }

  readonly FORMATIONS: Array<Formation> = [
    { date_debut: new Date("2018-09"), date_obtention: undefined, titre: "DUT Informatique", universite: "IUT A Paul Sabatier, Toulouse" },
    { date_obtention: new Date("2018-06"), titre: "Baccalauréat Section S, SVT, Spécialité ISN", ecole: " Lycée Pierre Bourdieu, Fronton" },
  ];

  // TODO Vérifier les donnéees.
  readonly EXPERIENCES: Array<Experience> = [
    { periode: { debut: new Date("2020-10"), duree: { qte: 1, unit: MOIS } }, sujet: "Calcul de distance de trajets aériens", technos: [PYTHON], equipe: { nbrPersonnes: 2 }, typeProjet: TypeProjet.SCOLAIRE },
    { periode: { debut: new Date("2020-08"), duree: { qte: 1, unit: MOIS } }, sujet: "Jeu de \"tower defense\" avec interface graphique", technos: [JAVA], equipe: { nbrPersonnes: 1 }, typeProjet: TypeProjet.PERSO },
    { periode: { debut: new Date("2020-06"), duree: { qte: 2, unit: MOIS } }, sujet: "Cryptage/décryptage de message par méthode \"César et Vigenère\"", technos: [C], equipe: { nbrPersonnes: 2 }, typeProjet: TypeProjet.SCOLAIRE },
    { periode: { debut: new Date("2020-01"), duree: { qte: 1, unit: MOIS } }, sujet: "Web documentaire interactif sur les ordinateurs du passé", technos: [HTML, CSS], equipe: { nbrPersonnes: 4 }, typeProjet: TypeProjet.SCOLAIRE },
    { periode: { debut: new Date("2020-01"), duree: { qte: 1, unit: MOIS } }, sujet: "Bot de jeu vidéo", technos: [JAVASCRIPT], equipe: { nbrPersonnes: 1 }, typeProjet: TypeProjet.PERSO },
    { periode: { debut: new Date("2020-01"), duree: { qte: 1, unit: MOIS } }, sujet: "Distributeur de billets, principaux design patterns, table de hachage et tests unitaires", technos: [JAVA], equipe: { nbrPersonnes: 2 }, typeProjet: TypeProjet.SCOLAIRE },
    { periode: { debut: new Date("2020-01"), duree: { qte: 2, unit: JOUR } }, sujet: "Résolution de Sudoku", technos: [ADA], equipe: { nbrPersonnes: 5 }, typeProjet: TypeProjet.SCOLAIRE },
    { periode: { debut: new Date("2020-01"), duree: { qte: 1, unit: SEMAINE } }, sujet: "Stage au Ministère de l'Agriculture", technos: [HTML], equipe: { nbrPersonnes: 4 }, typeProjet: TypeProjet.SCOLAIRE },
  ];
  readonly TECHNOLOGIES: Technologie[] = TECHNOLOGIES;

  readonly TITRE = 'CV ' + this.INFOS_GENERALES.prenom + ' ' + this.INFOS_GENERALES.nom

  readonly displayedColumns = ['debut', 'duree', 'sujet', 'technos', 'equipe', 'type'];
  readonly xpDataSource = new ExperienceTableDataSource(this.EXPERIENCES, this.datePipe);

  public set filtreTechno(filtreTechno: string) {
    this.filtrerXP(filtreTechno);
  }

  @ViewChild(MatSort)
  sort: MatSort | undefined;

  constructor(
    private titleService: Title,
    private datePipe: DatePipe,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    locationStrategy: LocationStrategy
  ) {
    this.titleService.setTitle(this.TITRE);

    this.BASE_HREF = (locationStrategy.getBaseHref() == '/' ? '' : locationStrategy.getBaseHref())

    // Cf. https://github.com/angular/components/issues/9728
    iconRegistry.addSvgIconInNamespace(
      'language',
      'python',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/lang-logo-python.svg'));

    // const svgUrl = 'assets/img/lang-logo-python.svg';
    // // domain and port for SSR in this example is static. Use i.e. environment files to use appropriate dev/prod domain:port
    // const domain = (isPlatformServer(PLATFORM_ID)) ? 'http://localhost:4000/' : '';
    // iconRegistry.addSvgIconInNamespace('language', 'python', sanitizer.bypassSecurityTrustResourceUrl(domain + svgUrl));

    // if (isPlatformServer(PLATFORM_ID)) {
    //   /* Register empty icons for server-side-rendering to prevent errors */
    //   iconRegistry.addSvgIconLiteralInNamespace('language', 'python', sanitizer.bypassSecurityTrustHtml('<svg></svg>'));
    // } else {
    //   iconRegistry.addSvgIconInNamespace('language', 'python', sanitizer.bypassSecurityTrustResourceUrl('/assets/img/lang-logo-python.svg'));
    // }
  }

  ngOnInit() {
    //this.displayGraph() // Does not work. Instead, lazy-load in this.displayGraph().
  }

  ngAfterViewInit() {
    this.xpDataSource.sort = this.sort!;
  }

  sliderLabel(value: number): string {
    const annee = Math.floor(value / 12)
    const mois = (value % 12) + 1

    const date = new Date(annee + "-" + mois)

    const datePipe = this.datePipe == undefined ? new DatePipe('fr') : this.datePipe

    const str: string | null = datePipe.transform(date, 'yyyy\nMMM')
    return str == null ? '?' : str
  }

  nomsTechnos(technos: Technologie[], sep: string): string {
    return technos.map((t: Technologie) => t.nom).join(sep);
  }

  getDuree = (xp: Experience): string => {
    const result = xp.periode.duree;
    return result === null ? '' : result.qte + ' ' + result.unit;
  }

  filtresActives = false

  filtrerXP(filterValue: string): void {
    this.xpDataSource.filter = filterValue;
  }
  defiltrerXP(): void {
    this.xpDataSource.filter = '';
  }
  filtrePose(): boolean {
    return this.xpDataSource.filter !== '';
  }

  trierXP(sort: Sort) {
    this.xpDataSource.sort = sort! as MatSort;
  }

  sujetHTML(xp: Experience): string {
    return sujetHTML(xp);
  }

  projetScolaire(type: TypeProjet): boolean {
    return type === TypeProjet.SCOLAIRE;
  }

  sequence(size: number): number[] {
    return Array.from(Array(size).keys());
  }

  technologies(): Technologie[] {
    const technologies: Technologie[] = this.EXPERIENCES.reduce((prev: Technologie[], curr: Experience, currIdx, arr) => prev.concat(curr.technos), [])
    return technologies;
  }


  onTabChanged(event: MatTabChangeEvent) {
    const tabName = event.tab.textLabel;

    this.titleService.setTitle(this.TITRE + " - " + tabName.charAt(0).toUpperCase() + tabName.slice(1).toLowerCase());

    if (tabName === 'competences') {
      this.displayGraph()
    }
  }

  private competencesGraph: CompetencesGraph | undefined;
  competencesGraphIsLoading = false;

  private displayGraph() {
    if (this.competencesGraph == undefined) {

      this.competencesGraphIsLoading = true;

      // Cf. https://blog.logrocket.com/data-visualization-angular-d3/
      this.competencesGraph = this.createCompetencesGraph();
      this.drawBars();

      // setTimeout(() => {
      this.competencesGraphIsLoading = false;
      // }, 5000);
    }
  }

  private createCompetencesGraph(): CompetencesGraph {
    const width = 750
    const height = 400
    const margin = { top: 50, right: 50, bottom: 50, left: 50 }
    const graph_width = width - (margin.left + margin.right)
    const graph_height = height - (margin.top + margin.bottom)

    const svg = d3.select("figure#competences_graph")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    return { svg: svg, margin: margin, width: graph_width, height: graph_height };
  }

  private drawBars(): void {

    const data: CompetencesGraphData[] = this.computeCompetencesData()
    const max: number = Math.max(...data.map(d => d.nbrJoursXP), 0) // d3.max(data, d => d.nbrJoursXP)

    // Create the X-axis band scale
    const x = d3.scaleBand()
      .domain(data.map((d) => d.nomTechno))
      .range([0, this.competencesGraph!.width])
      .padding(0.2)

    // Draw the X-axis on the DOM
    this.competencesGraph!.svg.append("g")
      .attr("transform", "translate(" + this.competencesGraph!.margin.left + "," + this.competencesGraph!.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end")
      .style("font-size", "12pt")

    // Create the Y-axis band scale
    const y = d3.scaleLinear()
      .domain([0, max]).nice()
      .range([this.competencesGraph!.height, 0])

    // Draw the Y-axis on the DOM
    this.competencesGraph!.svg.append("g")
      .attr("transform", "translate(" + this.competencesGraph!.margin.left + ", 0)")
      .call(d3.axisLeft(y))

    // Create and fill the bars
    this.competencesGraph!.svg.selectAll("competences_graph")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", (d: CompetencesGraphData) => x(d.nomTechno)! + this.competencesGraph!.margin.left)
      .attr("y", (d: CompetencesGraphData) => y(d.nbrJoursXP)! + this.competencesGraph!.margin.top)
      .attr("width", x.bandwidth())
      .attr("height", (d: CompetencesGraphData) => this.competencesGraph!.height - this.competencesGraph!.margin.top - y(d.nbrJoursXP))
      .attr("fill", "#d04a35");
  }

  // Cf. https://blog.logrocket.com/data-visualization-angular-d3/

  computeCompetencesData(): CompetencesGraphData[] {
    const data = new Array<CompetencesGraphData>()
    TECHNOLOGIES.forEach((techno: Technologie) => {
      const nbrJoursXP: number = this.EXPERIENCES
        .filter((xp: Experience) => xp.technos.some((t: Technologie) => t === techno))
        .map((xp: Experience) => xp.periode.duree)
        .reduce((sum, current) => sum + (current.qte * current.unit.nbrJours(current.unit)), 0)
      const datum: CompetencesGraphData = { nomTechno: techno.nom, nbrJoursXP: nbrJoursXP }
      data.push(datum)
      console.log("Data ++ " + techno.nom + " -> " + nbrJoursXP)
    });
    return data
  }
}

function typesProjet(): string[] {
  const typesProjet = []
  for (let value in TypeProjet) {
    typesProjet.push(value)
  }
  return typesProjet
}


const TYPE_PROJET_VALUES = typesProjet()

class ExperienceTableDataSource extends MatTableDataSource<Experience> {
  filterPredicate =
    (xp: Experience, filterValue: string) => {
      filterValue = filterValue.trim().toLowerCase();
      return (
        xp.periode.debut.toISOString().includes(filterValue)
        ||
        xp.sujet.toLowerCase().includes(filterValue)
        ||
        xp.technos.some((t: Technologie) => t.nom.toLowerCase().includes(filterValue))
        ||
        (!isNaN(Number(filterValue)) && xp.equipe.nbrPersonnes >= Number(filterValue))
        ||
        TYPE_PROJET_VALUES.some((tp: string) => xp.typeProjet.toLowerCase() == tp && tp.toLowerCase().includes(filterValue))
      )
    }
  sortingDataAccessor =
    (xp: Experience, sortHeaderId: string) => {
      const result: string | number | null | undefined =
        sortHeaderId === 'debut' ? this.datePipe.transform(xp.periode.debut, 'yyyy/MM') :
          sortHeaderId === 'duree' ? xp.periode.duree.qte :
            sortHeaderId === 'sujet' ? xp.sujet :
              sortHeaderId === 'equipe' ? xp.equipe.nbrPersonnes :
                sortHeaderId === 'type' ? xp.typeProjet :
                  undefined;
      if (result == undefined)
        throw Error('Colonne non gérée pour le tri : "' + sortHeaderId + '"')
      return result!
    }
  constructor(experiences: Array<Experience>, private datePipe: DatePipe) {
    super(experiences)
  }
}

interface CompetencesGraph {
  svg: any
  margin: { top: number, right: number, bottom: number, left: number }
  width: number
  height: number
}

interface CompetencesGraphData {
  nomTechno: string
  nbrJoursXP: number
}
